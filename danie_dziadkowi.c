#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

void error(int nr) {
    printf("\n\n\nERROR %d\n\n\n", nr);
    fflush(stdout);
    exit(1);
}

int main(int argc, char** argv) {
    int grandpa_pid = getpid();
    int rv = 0;

    switch (fork()) {
        case -1:
            error(53);
            break;
        case 0:
            // son
            switch (fork()) {
                case -1:
                    error(34);
                    break;
                case 0:
                    // grandson
                    rv = givekudos(grandpa_pid);

                    if (rv != -1) {
                        printf("zwrocil %d\n", rv);
                        error(105);
                    }

                    if (errno != EPERM) {
                        printf("errno sie nie zgodzilo\n");
                        error(345);
                    }

                    break;
                default:
                    wait(NULL);
            }
            break;
        default:
            wait(NULL);
    }

    printf("koniec testu\n");
    fflush(stdout);
    return 0;
}