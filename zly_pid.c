#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

void error(int nr) {
    printf("\n\n\nERROR %d\n\n\n", nr);
    fflush(stdout);
    exit(1);
}

int main(int argc, char** argv) {
    int rv = givekudos(53536563);
    if (rv != -1) {
        printf("zwrocil %d\n", rv);
        error(105);
    }

    if (errno != EINVAL) {
        printf("errno sie nie zgodzilo jest %d a mialo byc %d\n", errno, EINVAL);
        error(345);
    }

    printf("koniec testu\n");
    fflush(stdout);

    return 0;
}